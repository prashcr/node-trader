var request = require('request');
var apiUrl = 'http://128.199.74.105:2015';
var code = process.argv[2];
var teamDetails = {
  "teamName": "npm",
  "teamMembers": [
    "Samuel",
    "Kenta",
    "Prashanth",
    "John"
  ],
  "teamId": 10,
  "code": code
};

request.post({
  url: apiUrl + '/register',
  json: teamDetails
}, function(err, res, body) {
  request.post({
    url: apiUrl + '/redeem',
    json: {
      teamId: body.teamId,
      code: code
    }
  }, function(err, res, body) {
    console.log(body);
  });
});