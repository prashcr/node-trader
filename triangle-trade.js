var trade = require('./trade');

module.exports = function(curr, n) {
  trade(curr.substr(0, 6), n);
  trade(curr.substr(3, 6), n);
  trade(curr.substr(6, 3) + curr.substr(0, 3), n);
}
