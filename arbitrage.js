// node trader
var request = require('request');
var async = require('async');
var triangleTrade = require('./triangle-trade');
var teamId = 'bd6a16a1-9733-4258-a3cd-c3f7f845ae6b';
var currencies = ['CSC', 'AUD', 'USD', 'SGD', 'EUR'];
var currencyPairs = [];
var apiUrl = 'http://128.199.74.105:2015';
var counter = 0;

for (var i = 0; i < currencies.length; i++) {
  for (var j = 0; j < currencies.length; j++) {
    if (i != j) {
      currencyPairs.push(currencies[i] + currencies[j]);
    }
  }
}

function arbitrage() {
  var result = [];
  var tasks = currencyPairs.map(function(curr) {
    return function(callback) {
      request(apiUrl + '/fx/' + curr, function(err, res, body) {
        var data = JSON.parse(body);
        result.push({
          title: data.fxValue.currencyPair,
          rate: data.fxValue.fxRate
        });
        callback(null, data.fxValue.fxRate);
      });
    };
  });
  async.parallel(tasks, function() {
    function matchTrade(a, b) {
      return a.substr(3, 3) == b.substr(0, 3);
    }
    // All tasks are done now
    var arbit, passedArbit = [],
      compare = 0;

    for (var i = 0; i < result.length; i++) {
      for (var j = 0; j < result.length; j++) {
        if (matchTrade(result[i].title, result[j].title)) {
          for (var k = 0; k < result.length; k++) {
            if (matchTrade(result[j].title, result[k].title) && matchTrade(result[k].title, result[i].title)) {
              arbit = result[i].rate * result[j].rate * result[k].rate;
              if (arbit > 1.01) {
                var formattedTitle = result[i].title + result[j].title.substr(3, 3);
                triangleTrade(formattedTitle, 5000);
              }
            }

            // if(result[k].title.substr(0,3)==result[j].title.substr(3,3)
            //     && result[k].title.substr(3,3)==result[i].title.substr(0,3)
            //   && i != j && matchTrade(result[i].title, result[j].title)
            // ){
            //   arbit = result[i].rate * result[j].rate * result[k].rate;
            //   if(arbit>1){
            //     passedArbit.push({
            //       title: result[i].title + result[j].title.substr(3, 3),
            //       arbitRate: arbit
            //     });
            //   }
            // }
            // if (i != j && matchTrade(result[i].title, result[j].title)) {
            //   arbit = result[i].rate * result[j].rate;
            //   baseTrade = getBaseTrade(result[i].title, result[j].title);
            //   compare = arbit > baseTrade.rate;
            //   if (arbit>1 && compare) {
            //     //if(sameEndpoint(arbit,baseTrade.title)){
            //     passedArbit.push({
            //       title: result[i].title + result[j].title.substr(3, 3),
            //       arbitRate: arbit
            //     });
            //     //passedArbit.push(getEndtoEnd(result[i].title.substring(0,3)+result[j].title.substring(3,6)));
            //     //passedArbit.push('Make the trade!');
            //     //}
            //   }
            // }
          }
        }
      }
    }
    // passedArbit.forEach(function  (tri) {
    //   console.log('arbitRate'+tri.arbitRate);
    //
    // });
    counter++;
    passedArbit = [];
    if (counter < 120) setTimeout(arbitrage, 100 / 4)
      // console.log(passedArbit);
      //console.log(allArbit);
  });
}
arbitrage();