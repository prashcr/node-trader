var request = require('request');
var async = require('async');
var teamId = 'bd6a16a1-9733-4258-a3cd-c3f7f845ae6b';
var apiUrl = 'http://128.199.74.105:2015';
//require('request').debug = true

module.exports = function executeTrade(pair, quantity) {
  async.waterfall([
    function(callback) {
      request(apiUrl + '/fx/' + pair, function(err, res, body) {
        try {
          var rate = JSON.parse(body).fxValue.fxRate;
          console.log('rate' + rate);
          quantity *= rate;
          console.log('quant' + quantity);
        }
        catch(e) {
          console.log(e.message);
        }
        finally {
          callback(null);
        }
      });
    },
    function(callback) {
      request.post({
        url: apiUrl + '/fx/quote',
        json: {
          teamId: teamId,
          currencyPair: pair,
          quantity: quantity
        }
      }, {
        timeout: 1500
      }, function(err, res, body) {
        // console.log('res'+res);
        // console.log(err.code);
        // console.log(err.connect === true);
        if (!body) {
          callback(null, 'error');
        } else {
          var quoted = body.quoteResponse.fxRate;
          var quoteId = body.quoteResponse.quoteId;
          //console.log('Quoted price is', body.quoteResponse.fxRate);
          callback(null, quoteId);
        }
      });
    },
    function(quoteId, callback) {
      request.post({
        url: apiUrl + '/fx/quote/execute',
        json: {
          teamId: teamId,
          quoteId: quoteId
        }
      }, function(err, res, body) {
        callback(null, 'done');
      });
    }
  ], function(err, result) {
    request(apiUrl + '/account/balance/4b606b42-d70f-4960-bd1d-da3aaff52355',
      function(err, res, body) {
        console.log('Remaining balance:');
        console.log(body);
      });
  });
}