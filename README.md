node-trader
===========

Node.js implementation of algorithmic forex trader built for CodeIT Suisse.
Based on the principle of triangular arbitrage.

Attempted to enforce separation of concerns by making use of separate modules with well-defined inputs

Shortfalls
----------

- Our trade module simply queried for a quote then executed it in a single step without trying to get better quotes.
- Our algorithm wasn't mathematically rigorous. Instead of trying to loop through every possible currency combination, we should've used a graph traversal algorithm such as A\* or DFS. 
