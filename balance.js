var request = require('request');
var async = require('async');
var apiUrl = 'http://128.199.74.105:2015';
var teamId = 'bd6a16a1-9733-4258-a3cd-c3f7f845ae6b';

request(apiUrl + '/account/balance/' + teamId, function(err, res, body) {
  var balance = JSON.parse(body);
  var tasks;
  var currencies = [
    'AUD',
    'USD',
    'EUR',
    'SGD'
  ];

  tasks = currencies.map(function(curr) {
    return (function(callback) {
      request(apiUrl + '/fx/' + curr + 'CSC', function(err, res, body) {
        try {
          var rate = JSON.parse(body).fxValue.fxRate;
          var value = rate * balance[curr];
        } catch(e) {
          console.log(e.message);
        }
        callback(null, value);
      });
    });
  });

  async.parallel(tasks,
    function(err, results) {
      var total = results.reduce(function(sum, val) {
        return sum + val;
      }, balance.CSC);
      console.log('Final CSC balance:', total);
    });
});
